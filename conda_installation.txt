# Download the Miniconda file
# chmod 777 Miniconda.....file.sh

# Install Miniconda
bash Miniconda....file.sh

# Update conda
conda update conda

# Create conda environment 
conda create -n myenvname python=3.7 anaconda


# Activate your virtual environment
source activate myenvname


# deactivate virtual environment
source deactivate

# Install packages inside the virtual environment
conda install -n yourenvname [package]

# Delete a virtual environment 
conda remove -n yourenvname -all


# Install from requirements.txt
while read requirement; do conda install --yes $requirement; done < requirements.txt


Remove kernels from jupyter notebook
--------------------------------------
rm -r ~/.local/share/jupyter/kernels/nameofkernel


Make the environment available in Jupyter notebook
-------------------------------------------------
conda install -n yourenvname tornado
conda install -n yourenvname ipykernel
ipython kernel install --user --name=nameofenv



##############################################
INSTALL TENSORFLOW-GPU and CUDA toolkit and cuDNN altogether with 1 line using conda

conda create --name nameofenv tensorflow-gpu

# This will install all the requirements needed to run tensorflow-gpu in Nvidia GPU card 


